@echo off
title InsanityX - Loading
color 0a
cd bin

:start
echo.
echo ------------------------------------------------
echo     InsanityX (x64), Starting up...
echo ------------------------------------------------
if exist "%HOMEDRIVE%\Program Files (x86)"  goto x64
goto x86


:x64
title  InsanityX (x64) - Loader
echo         Auto-Detected 64-bit (x64) System
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo    . C l i e n t   o u t p u t   b e l o w .
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo.
"%HOMEDRIVE%/Program Files (x86)/Java/jre7/bin/java.exe" -Xmx400m Jframe 10 0 highmem members 32


:x86
title  InsanityX (x86) - Loader
echo         Auto-Detected 32-bit (x86) System
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo    . C l i e n t   o u t p u t   b e l o w .
echo ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
echo.

"%HOMEDRIVE%/Program Files/java/jre7/bin/java.exe" -Xmx400m Jframe 10 0 highmem members 32
pause